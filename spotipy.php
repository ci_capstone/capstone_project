<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<style>
body {
  background-image: url('background.jpg');
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-size: cover;
}
</style>
</head>
<body>

<div style='border: 2px solid gray; border-radius: 25px; background-color: #f2f2f2; padding: 20px; width: 550px; margin-left: auto; margin-right: auto; text-align: center;'>
<h1 style="text-align: center;"><u>Top 10 - USA Songs of the Week</u><h1>
<h2 style="text-align: center;">By: James Mc Ginty, Rodman Bourne, Tyler Stanley<h2>
</div>

    <?php
                $host= "127.0.0.1";
                $port= "3306";
				$servername = "localhost";
                $username = "root";
                $password = "";
                $dbname = "spotify";

                // Create connection
                $conn = new mysqli($servername, $username, $password, $dbname);
                // Check connection
                if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
                }
                $sql = "SELECT album_url, album_image, album_release_date, lyrics_body, album_name, track_name, artist_name FROM spotipy";
                $result = $conn->query($sql);

                if ($result->num_rows > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) {
						
						$album_str = $row["album_url"];
						$album_str = substr($album_str, 31);
						
						$str = "https://open.spotify.com/embed/track/";
						$str .= $album_str;
						
                        echo "<br><div style='border: 2px solid gray; background-color: #f2f2f2; padding: 20px; width: 700px; margin-left: auto; margin-right: auto; text-align: center;'><h2><u>'<b>". $row["album_name"]. "'</b> || '". $row["track_name"]. "' || '". $row["artist_name"]. "'</b></u></h2>". '<img src="'.$row["album_image"].'" style="border-radius: 50px; width: 15%;"><br>'.  "<b><h3>Album Release Date:</h3></b>" . $row["album_release_date"]. "<br><br><div style='border: 2px solid black; border-radius: 25px; background-color: white; padding: 20px; width: auto; margin-left: auto; margin-right: auto; text-align: center;'><b><h3>Song Lyrics:</h3></b><center><details style='width:400px;'><summary>Expand to View</summary>". $row["lyrics_body"]. '...</details></center></div><br><iframe src="'.$str.'" width="697" height="80" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe></div><br></details>';

						
                    }
                } else {
                    echo "0 results";
                }
                $conn->close();
            ?>

  
  <p>All content and graphics on this web site are the property of the company Refsnes Data.</p>

</body>

</html>