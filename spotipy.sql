-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 13, 2020 at 09:20 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spotify`
--

-- --------------------------------------------------------

--
-- Table structure for table `spotipy`
--

CREATE TABLE `spotipy` (
  `id` varchar(255) DEFAULT NULL,
  `album_url` varchar(255) DEFAULT NULL,
  `album_image` varchar(255) DEFAULT NULL,
  `album_release_date` varchar(255) DEFAULT NULL,
  `lyrics_body` varchar(255) DEFAULT NULL,
  `album_name` varchar(255) DEFAULT NULL,
  `track_name` varchar(255) DEFAULT NULL,
  `artist_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `spotipy`
--

INSERT INTO `spotipy` (`id`, `album_url`, `album_image`, `album_release_date`, `lyrics_body`, `album_name`, `track_name`, `artist_name`) VALUES
('1', 'https://open.spotify.com/track/127QTOFJsJQp5LbJbu3A1y', 'https://i.scdn.co/image/ab67616d0000b2736443676b54522a86f6323e65', '2020-04-03', 'Yeah\n\nI\'ve been tryna call\nI\'ve been on my own for long enough\nMaybe you can show me how to love, maybe\nI\'m going through withdrawals\nYou don\'t even have to do too much\nYou can turn me on with just a touch, baby\n\nI look around and\nSin City\'s cold and empt', 'After Hours', 'Blinding Lights', 'The Weeknd'),
('2', 'https://open.spotify.com/track/0VjIjW4GlUZAMYd2vXMi3b', 'https://i.scdn.co/image/ab67616d0000b2738863bc11d2aa12b54f5aeb36', '2020-03-20', 'Panic on the brain, world has gone insane\nThings are starting to get heavy, mm\nI can\'t help but think I haven\'t felt this way\nSince I asked you to go steady\n\nWonderin\'\nWould you be\nMy little quarantine?\nOr is this the way it ends?\n\n\'Cause I told you my le', 'Level of Concern - Single', 'Level of Concern', 'twenty one pilots'),
('3', 'https://open.spotify.com/track/0nbXyq5TXYPCO7pr3N8S4I', 'https://i.scdn.co/image/ab67616d0000b273600adbc750285ea1a8da249f', '2019-12-06', 'Black leather glove, no sequins\nBuckles on the jacket, it\'s Alyx shit\nNike crossbody, got a piece in it\nGot a dance, but it\'s really on some street shit\nImma show you how to get it\n\nIt go right foot up, left foot, slide\nLeft foot up, right foot, slide\nBas', 'Toosie Slide - Single', 'Toosie Slide', 'Drake'),
('4', 'https://open.spotify.com/track/22LAwLoDA5b4AaGSkg6bKW', 'https://i.scdn.co/image/ab67616d0000b2730824105a6282782aaabb0584', '2020-02-06', 'Are you one of them girls?\nThat peels off the bud light label\nJust might run a pool table\nRoll your eyes if I call you an angel\n\nAin\'t you, one of them girls?\nAsked you to dance, you say \"no\"\nJust to see how far I\'ll go\nYour song comes on and your eyes cl', 'One of Them Girls - Single', 'One of Them Girls', 'Lee Brice'),
('5', 'https://open.spotify.com/track/24Yi9hE78yPEbZ4kxyoXAI', 'https://i.scdn.co/image/ab67616d0000b273e673050c403376e1e95c6980', '2019-10-09', 'I won\'t say I\'m sorry over and over\nCan\'t just say I\'m sorry, I\'ve gotta show you\nI won\'t do it again, I\'ll prove my love is true\nI hope the last time I said sorry\nIs the last time I\'ll say sorry to you\n\nThe first time I slept on the couch, was our first ', 'Last Time I Say Sorry - Single', 'Last Time I Say Sorry', 'Kane Brown & John Legend'),
('6', 'https://open.spotify.com/track/3PfIrDoz19wz7qK7tYeu62', 'https://i.scdn.co/image/ab67616d0000b273c966c2f4e08aee0442b6b8d6', '2020-03-27', 'Don\'t have to leave this town to see the world\n\'Cause it\'s something that I gotta do\nI don\'t wanna look back in 30 years\nAnd wonder who you\'re married to\n\nWanna say it now, wanna make it clear\nFor only you and God to hear\nWhen you love someone, they say y', 'Fully Loaded: God\'s Country', 'Nobody But You (feat. Gwen Stefani)', 'Blake Shelton'),
('7', 'https://open.spotify.com/track/1jaTQ3nqY3oAAYyCTbIvnM', 'https://i.scdn.co/image/ab67616d0000b27305a448540b069450ccfba889', '2020-03-13', 'I want a boyfriend\nBut I just keep hitting dead ends\nTry to take a shortcut\nBut I get cut again and again\nI want a boyfriend\n\nTell me, are there any good ones left?\nI keep finding wrong ones\nBut I want love again and again\nI want a boyfriend\n\nI been up al', 'Rare (Deluxe)', 'Boyfriend', 'Selena Gomez'),
('8', 'https://open.spotify.com/track/3Dv1eDb0MEgF93GpLXlucZ', 'https://i.scdn.co/image/ab67616d0000b27382b243023b937fd579a35533', '2019-11-07', 'I\'m that bitch (Yeah)\nBeen that bitch, still that bitch (Ah)\nWill forever be that bitch (Forever be that bitch)\nYeah (Ayy, ah)\n\nI\'m the hood Mona Lisa, break a nigga into pieces\nHad to X some cheesy niggas out my circle like a pizza (Yeah)\nI\'m way too exc', 'Suga', 'Savage', 'Megan Thee Stallion'),
('9', 'https://open.spotify.com/track/55CHeLEfn5iJ0IIkgaa4si', 'https://i.scdn.co/image/ab67616d0000b27320fbc17fcb9376bc76a1b510', '2020-03-06', 'In a time full of war, be peace\nIn a time full of doubt, just believe\nYeah, there ain\'t that much difference between you and me\nIn a time full of war, be peace\n\nIn a world full of hate, be a light\nWhen you do somebody wrong, make it right\nDon\'t hide in th', 'Be a Light (feat. Reba McEntire, Hillary Scott, Chris Tomlin & Keith Urban) - Single', 'Be a Light (feat. Reba McEntire, Hillary Scott, Chris Tomlin & Keith Urban)', 'Thomas Rhett'),
('10', 'https://open.spotify.com/track/7eJMfftS33KTjuF7lTsMCx', 'https://i.scdn.co/image/ab67616d0000b273bf01fd0986a195d485922167', '2020-02-08', 'They say \"Oh my god I see the way you shine\"\nTake your hand, my dear, and place them both in mine\nYou know you stopped me dead while I was passing by\nAnd now I beg to see you dance just one more time\n\nOoh I see you, see you, see you every time\nAnd oh my I', 'The Kids Are Coming - EP', 'Dance Monkey', 'Tones and I');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
