# Rowan University Computing & Informatics Capstone Project
Rowan University Computing & Informatics Capstone Project Repository

Purpose of this project was to create a website which demonstrated interacting with a combination of 3 APIs. Each group member was assigned an API to integrate into the project. The project utilized Python, MySQL, HTML, and CSS, paritcipants included James Mc Ginty (Spotify API), Rodman Bourne (Billboard 100), Tyler Stanley (Itunes).

Final Result: https://gitlab.com/ci_capstone/capstone_project/-/blob/master/unknown.png

APIs: Itunes, Spotify, Billboard 100
